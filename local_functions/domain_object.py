from xpms_helper.model import domain_util


def domain_object(**kwargs):
    obj = domain_util.get_domain_object({"solution_id": "18481234", "doc_id": "1ed57bd2-2b34-42c0-a35b-f92faa7e9b9b", "root_id": "1ed57bd2-2b34-42c0-a35b-f92faa7e9b9b"})
    print("domain object = ", obj)
    return obj

domain_object()