from xpms_helper.executions.execution_variables import ExecutionVariables


def set_execution_variables(**kwargs):
    # context = {}
    context = {"execution_id": "VDqvo8OWRp2pMQ3814l1JQ", "solution_id": "18481234"}
    exec_ins = ExecutionVariables.get_instance(context)
    output = exec_ins.set_variable('df', 'uma')
    print(output)

    return kwargs


def get_execution_variables(**kwargs):
    # context = {}
    context = {"execution_id": "VDqvo8OWRp2pMQ3814l1JQ", "solution_id": "18481234"}
    exec_ins = ExecutionVariables.get_instance(context)
    output = exec_ins.get_variable('df')

    return output,kwargs


set_execution_variables()
get_execution_variables()