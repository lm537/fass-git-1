from xpms_helper.executions.execution_variables import ExecutionVariables


def get_execution_variable(config,**kwargs):
    context = config["context"]
    exec_ins = ExecutionVariables.get_instance(context)
    output = exec_ins.get_variable('set_vars')
    return output,kwargs


