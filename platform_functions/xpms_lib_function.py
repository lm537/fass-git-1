from xpms_helper.model import domain_util,document_util,recommendation_util


def xpms_lib_function(config, **kwargs):
    doc_id = config["context"]["doc_id"]
    solution_id = config["context"]["solution_id"]
    payload = {"solution_id": solution_id, "doc_id": doc_id, "root_id": doc_id}
    document = document_util.get_document_object(payload)
    domain = domain_util.get_domain_object(payload)
    recommendation = recommendation_util.get_recommendation_object(payload)
    return {"document": document, "domian" : domain, "recommendation" : recommendation}
