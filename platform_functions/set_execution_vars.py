from xpms_helper.executions.execution_variables import ExecutionVariables


def set_execution_vars(config,**kwargs):
    context = config["context"]
    exec_ins = ExecutionVariables.get_instance(context)
    exec_ins.set_variable('set_vars', 'uma_mahesh')
    return kwargs
