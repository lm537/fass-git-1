from xpms_helper.model import model_utils
#we should pass the env variable is EXECUTION_LOCAL_PATH:/media/xmps/UMA/sample/sample_project/functions/
file_name = 'model.pkl'


def load_pkl_file(config,**kwargs):
    if not "src_dir" in config:
        config["src_dir"] = "18481234"
    if "storage" not in config:
        config["storage"] = ""
    pk_file = model_utils.load(file_name, config)
    assert pk_file is not None
    return kwargs
# load_pkl_file(config={})